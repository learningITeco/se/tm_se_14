package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.util.UUID;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstracEntity{

	@Id
	@Nullable
	private String id = UUID.randomUUID().toString();

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
