package ru.potapov.tm.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.Status;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public class Project extends AbstracEntity implements Cloneable, Serializable {
    @Id
    @Nullable private String id = UUID.randomUUID().toString();

    @Column(name = "name", unique = true)
    @Nullable private String      name;

    @Column(name = "description")
    @Nullable private String      description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @Nullable private User      user;

    @Column(name = "dateBegin")
    @Nullable private Date dateStart;

    @Column(name = "dateEnd")
    @Nullable private Date dateFinish;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    @Nullable private Status status;

    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> items = new ArrayList<>();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
