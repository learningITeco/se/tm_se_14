package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.AbstracEntity;

import javax.persistence.EntityManager;
import java.util.Collection;

public interface IRepository<T extends AbstracEntity> {

    @Nullable T findOne(@NotNull final String name, @NotNull  EntityManager em);

    @NotNull Collection<T> findAll(@NotNull  EntityManager em);

    void persist(@NotNull final T t, @NotNull  EntityManager em);

    void merge(@NotNull final T t, @NotNull  EntityManager em);

    void remove(@NotNull final T t, @NotNull  EntityManager em) ;

    void removeAll(@NotNull  EntityManager em);
}
