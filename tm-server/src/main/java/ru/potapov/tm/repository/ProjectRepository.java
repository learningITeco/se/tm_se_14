package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Collection;

@Setter
@Getter
@NoArgsConstructor
//@RequestScoped
public class ProjectRepository implements IRepository<Project>, Serializable {

    @Nullable
    public Project findOneById(@NotNull final String id, @NotNull  EntityManager em){
        return em.find(Project.class, id);
    }

    @NotNull public Collection<Project> findAll(@NotNull  EntityManager em){
        return em.createQuery("From Project").getResultList();
    }

    @NotNull public Collection<Project> findAllByUserId(@NotNull final String userId, @NotNull  EntityManager em){
        return em.createQuery("From Project Where user_id =: userId").getResultList();
    }

    @Nullable public Project findOne(@NotNull final String name, @NotNull  EntityManager em){
        Project project = null;
        if (em.createQuery("from Project where name =:nameParam").setParameter("nameParam", name).getResultList().size() > 0)
            project = (Project)em.createQuery("from Project where name =:nameParam").setParameter("nameParam", name).getSingleResult();
        return project;
    }

    @Nullable public Project findOneByNameByUserId(@NotNull final String userId, @NotNull final String name, @NotNull  EntityManager em){
        return em.find(Project.class, userId);
    }

    public void persist(@NotNull final Project project, @NotNull EntityManager em){
        em.persist(project);
    }

    public void merge(@NotNull final Project project, @NotNull  EntityManager em){
        em.merge(project);
    }

    public void remove(@NotNull final Project project, @NotNull  EntityManager em) {
        em.remove(project);
    }

    public void removeAll(@NotNull  EntityManager em){
        em.createQuery("DELETE FROM Project ");
    }
}
