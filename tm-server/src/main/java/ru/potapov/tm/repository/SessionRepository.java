package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Session;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Named("SessionRepository")
public class SessionRepository implements IRepository<Session> {

    @NotNull public Collection<Session> findAll(@NotNull  EntityManager em){
        return em.createQuery("From Session").getResultList();
    }

    @Nullable public Session findOne(@NotNull final String id, @NotNull  EntityManager em){
        return em.find(Session.class, id);
    }

    public void persist(@NotNull final Session t, @NotNull EntityManager em){
        em.persist(t);
    }

    public void merge(@NotNull final Session t, @NotNull  EntityManager em){
        em.merge(t);
    }

    public void removeAll(@NotNull  EntityManager em){
        em.createQuery("DELETE FROM Session ");
    }

    @Override
    public void remove(@NotNull Session t, @NotNull  EntityManager em) {
        em.remove(t);
    }
}
