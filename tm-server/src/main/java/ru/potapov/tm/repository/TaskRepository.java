package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Task;

import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.Collection;

@Setter
@Getter
@NoArgsConstructor
@Named("TaskRepository")
public class TaskRepository implements IRepository<Task>{

    @Nullable public Collection<Task> findAll(@NotNull  EntityManager em){
        return em.createQuery("From Task").getResultList();
    }


    @NotNull public Collection<Task> findAllByUser(@NotNull final String userId, @NotNull  EntityManager em){
        return em.createQuery("From Task Where user_id =: userId").getResultList();
    }


    @Nullable public  Collection<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId, @NotNull  EntityManager em){
        return em.createQuery("From Task Where user_id =: userId AND project_id =: projectId").getResultList();
    }


    @Nullable public Task findOneById(@NotNull String id, @NotNull  EntityManager em){
        return em.find(Task.class, id);
    }


    @Nullable public Task findOne(@NotNull String name, @NotNull  EntityManager em){
        Task task = null;
        if (em.createQuery("from Task where name =:nameParam").setParameter("nameParam", name).getResultList().size() > 0)
            task = (Task)em.createQuery("from Task where name =:nameParam").setParameter("nameParam", name).getSingleResult();
        return task;
    }


    @Nullable public Task findOneByNameAndUserId(@NotNull final String userId, @NotNull final String name, @NotNull  EntityManager em) {
        return em.find(Task.class, name);
    }

    @Override
    public void remove(@NotNull final Task task, @NotNull  EntityManager em) {
        em.remove(task);
    }


    public void  persist(@NotNull final Task task, @NotNull EntityManager em) {
        em.persist(task);
    }

    public void merge(@NotNull final Task task, @NotNull  EntityManager em){
        em.merge(task);
    }

    public void removeAll(@NotNull  EntityManager em){
        em.createQuery("DELETE FROM Task ");
    }
}
