package ru.potapov.tm.repository;

import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.HibernateToMySql;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Collection;

@NoArgsConstructor
public class UserRepository implements IRepository<User>, Serializable {

    @Nullable public User findOne(@NotNull final String login, @NotNull  EntityManager em){
        User user = null;
        if (em.createQuery("from User where login =:loginParam").setParameter("loginParam", login).getResultList().size() > 0)
            user = (User)em.createQuery("from User where login =:loginParam").setParameter("loginParam", login).getSingleResult();
        return user;
    }

    @Nullable public User findOneById(@NotNull final String id, @NotNull  EntityManager em){
        return em.find(User.class, id);
    }

    @NotNull public Collection<User> findAll(@NotNull  EntityManager em){
        //return em.createQuery("From app_user").getResultList();
        return em.createQuery("From User").getResultList();
    }

    @Override
    public void persist(@NotNull User t, @NotNull EntityManager em) {
        em.persist(t);
    }

    @Override
    public void merge(@NotNull User t, @NotNull  EntityManager em) {
        em.merge(t);
    }

    @Override
    public void remove(@NotNull User t, @NotNull  EntityManager em) {
        em.remove(t);
    }

    @Override
    public void removeAll(@NotNull  EntityManager em) {
        em.createQuery("DELETE FROM User ");
    }
}
