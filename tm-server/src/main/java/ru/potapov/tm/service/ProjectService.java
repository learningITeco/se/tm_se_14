package ru.potapov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.DataXml;
import ru.potapov.tm.dto.Task;
import ru.potapov.tm.dto.User;
import ru.potapov.tm.dto.Project;
import ru.potapov.tm.repository.IRepository;
import ru.potapov.tm.repository.ProjectRepository;
import ru.potapov.tm.util.HibernateToMySql;
import ru.potapov.tm.util.ValidateExeption;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor
//@RequestScoped
@WebService(endpointInterface = "ru.potapov.tm.endpoint.IProjectEndpoint")
public class ProjectService extends AbstractService<Project> implements IProjectService {
    @Inject
    @Nullable private ProjectRepository repository;

    public ProjectService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository repository) {
        super(serviceLocator);
    }

    @Override
    public int checkProjectSize() throws ValidateExeption {
        System.out.println(repository);
        int i = 0;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            i = getRepository().findAll(entityManager).size();
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return i;
    }

    @Override
    public @Nullable Project findOneProject(@NotNull String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            project = entityToDto(getRepository().findOne(name, entityManager));
             entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}

        return project;
    }

    @Override
    public @Nullable Project findOneProjectById(@NotNull String id) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            project = entityToDto(getRepository().findOneById(id, entityManager));
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return project;
    }

    @Override
    public @Nullable Project findOneProjectByNameAndUserId(@NotNull String userId, @NotNull String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            project = entityToDto(getRepository().findOneByNameByUserId(userId, name, entityManager));
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return project;
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            project = entityToDto(getRepository().findOne(name, entityManager));
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return project;
    }

    @Nullable
    @Override
    public Project findProjectByNameAndUserId(@NotNull final String userId, @NotNull final String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            project = entityToDto(getRepository().findOneByNameByUserId(userId, name, entityManager));
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getProjectCollection(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        @Nullable  final User user = getServiceLocator().getUserService().getUserById( userId);
        boolean isAdmin = getServiceLocator().getUserService().isAdministrator( user);
        @Nullable Collection<Project> list = new ArrayList<>();
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            if ( isAdmin )
                list = collectionEntityToDto(getRepository().findAll(entityManager));
            else
                list =  collectionEntityToDto(getRepository().findAllByUserId(userId, entityManager));
             entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return list;
    }

    @NotNull
    public Collection<Project> getProjectCollection() throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return new ArrayList<>();

        @Nullable Collection<Project> list = new ArrayList<>();
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            list = collectionEntityToDto(getRepository().findAll(entityManager));
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return list;
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(name) && Objects.nonNull(getRepository()) ){
            @Nullable Project newProject = (Project) project.clone();
            newProject.setName(name);
            try {
                entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
                getRepository().merge(dtoToEntity(newProject), entityManager);
                entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
            finally {
                try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        }
        return project;
    }

    @Override
    public void removeAllProjectByUserId(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            for (ru.potapov.tm.entity.Project project : getRepository().findAllByUserId(userId, entityManager)) {
                if (userId.equals(project.getUser().getId()))
                    getRepository().remove(project, entityManager);
            }
             entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }
    
    @Override
    public void removeAllProject(@NotNull final Collection<Project> listProjects) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            for (Project project : listProjects) {
                getRepository().remove(dtoToEntity(project), entityManager);
            }
             entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }
    
    @Override
    public void removeProject(@NotNull final Project project) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            getRepository().remove(getRepository().findOneById(project.getId(), entityManager), entityManager);
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }
    
    @Override
    public void putProject(@NotNull final Project project) throws ValidateExeption{
        if (Objects.isNull(getRepository()))
            return;

        ru.potapov.tm.entity.Project entityProject = dtoToEntity(project);
        boolean exist = Objects.nonNull(findOneProjectById( project.getId()));
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            if (exist)
                getRepository().merge(entityProject, entityManager);
            else
                getRepository().persist(entityProject, entityManager);
            entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull final Project project, @NotNull final String owener) throws ValidateExeption{
        @NotNull String res = "";

        if (Objects.isNull(project.getDateStart()) || Objects.isNull(project.getDateFinish()))
            return res;

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Status: " + project.getStatus() + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + getServiceLocator().getFt().format(project.getDateStart() ) + "\n";
        res += "Date finish: " +  getServiceLocator().getFt().format(project.getDateFinish()) + "\n";

        return res;
    }

    //Save-Load
    @Override
    public void saveBinar() throws Exception{
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));

        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(collectionDtoToEntity(getProjectCollection()));
        dataXml.getListValueTask().addAll( getServiceLocator().getTaskService().collectionDtoToEntity(getServiceLocator().getTaskService().findAllTasks("")));

        inputStream.writeObject( dataXml );
        inputStream.close();
    }

    @Override
    public void loadBinar() throws Exception{
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        if (!file.canRead()){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
        @NotNull final Object dataObj = inputStream.readObject();
        if (dataObj instanceof DataXml) {
            @NotNull final DataXml data = (DataXml) dataObj;

            if (data.getListValueProject().size() > 0 ){
                for (int i = 0; i < data.getListValueProject().size(); i++) {
                    putProject(entityToDto(data.getListValueProject().get(i)));
                }
            }

            if ( data.getListValueTask().size() > 0){
                for (int i = 0; i < data.getListValueTask().size(); i++) {
                    getServiceLocator().getTaskService().putTask(getServiceLocator().getTaskService().entityToDto(data.getListValueTask().get(i)));
                }
            }
        }
    }
    @Override
    public void saveJaxb(boolean formatXml) throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(collectionDtoToEntity(getProjectCollection()));
        dataXml.getListValueTask().addAll( getServiceLocator().getTaskService().collectionDtoToEntity(getServiceLocator().getTaskService().findAllTasks("")));

        @NotNull final JAXBContext context      = JAXBContext.newInstance(DataXml.class) ;
        @NotNull final Marshaller marshaller    = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull String typeFormatName = "xml";
        if (!formatXml){
            typeFormatName = "json";
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        }
        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        marshaller.marshal(dataXml, new FileWriter(file));
    }
    @Override
    public void loadJaxb(boolean formatXml) throws Exception {
        @NotNull final JAXBContext context = JAXBContext.newInstance(DataXml.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();

        @NotNull String typeFormatName = "xml";
        if (!formatXml) {
            typeFormatName = "json";
            unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        }

        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        @NotNull final DataXml data =(DataXml) unmarshaller.unmarshal(file);

        if (data.getListValueProject().size() > 0 ){
            for (int i = 0; i < data.getListValueProject().size(); i++) {
                putProject(entityToDto(data.getListValueProject().get(i)));
            }
        }

        if ( data.getListValueTask().size() > 0){
            for (int i = 0; i < data.getListValueTask().size(); i++) {
                getServiceLocator().getTaskService().putTask(getServiceLocator().getTaskService().entityToDto(data.getListValueTask().get(i)));
            }
        }
    }

    @Override
    public void saveFasterXml() throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(collectionDtoToEntity(getProjectCollection()));
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().collectionDtoToEntity(getServiceLocator().getTaskService().findAllTasks("")));

        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper mapper  = new XmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dataXml);
    }
    @Override
    public void loadFasterXml() throws Exception {
        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);

        @NotNull Map<String, Project> mapProject = new HashMap<>();
        @NotNull Map<String, Task> mapTask       = new HashMap<>();
        if (data.getListValueProject().size() > 0 ){
            for (int i = 0; i < data.getListValueProject().size(); i++) {
                putProject(entityToDto(data.getListValueProject().get(i)));
            }
        }

        if ( data.getListValueTask().size() > 0){
            for (int i = 0; i < data.getListValueTask().size(); i++) {
                getServiceLocator().getTaskService().putTask(getServiceLocator().getTaskService().entityToDto(data.getListValueTask().get(i)));
            }
        }
    }

    @Override
    public void saveFasterJson() throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(collectionDtoToEntity(getProjectCollection()));
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().collectionDtoToEntity(getServiceLocator().getTaskService().findAllTasks("")));

        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dataXml);
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(s.getBytes());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
    @Override
    public void loadFasterJson() throws Exception {
        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);
        if (data.getListValueProject().size() > 0 ){
            for (int i = 0; i < data.getListValueProject().size(); i++) {
                putProject(entityToDto(data.getListValueProject().get(i)));
            }
        }

        if ( data.getListValueTask().size() > 0){
            for (int i = 0; i < data.getListValueTask().size(); i++) {
                getServiceLocator().getTaskService().putTask(getServiceLocator().getTaskService().entityToDto(data.getListValueTask().get(i)));
            }
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<ru.potapov.tm.entity.Project> collectionDtoToEntity(Collection<ru.potapov.tm.dto.Project> collectionProjectDto){
        Collection<ru.potapov.tm.entity.Project> list = new ArrayList<>();
        for (Project project : collectionProjectDto) {
            list.add(dtoToEntity(project));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<ru.potapov.tm.dto.Project> collectionEntityToDto(Collection<ru.potapov.tm.entity.Project> collectionProjectEntity){
        Collection<ru.potapov.tm.dto.Project> list = new ArrayList<>();
        for (ru.potapov.tm.entity.Project project : collectionProjectEntity) {
            list.add(entityToDto(project));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public ru.potapov.tm.entity.Project dtoToEntity(ru.potapov.tm.dto.Project projectDto){
        ru.potapov.tm.entity.Project projectEntity = new ru.potapov.tm.entity.Project();
        projectEntity.setId(projectDto.getId());
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            projectEntity = getRepository().findOneById(projectDto.getId(), entityManager);
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}


        if (Objects.isNull(projectEntity)){
            projectEntity = new ru.potapov.tm.entity.Project();
            projectEntity.setId(projectDto.getId());
            projectEntity = new ru.potapov.tm.entity.Project();
            projectEntity.setDescription(projectDto.getDescription());
            projectEntity.setName(projectDto.getName());
            projectEntity.setStatus(projectDto.getStatus());
            projectEntity.setUser( getServiceLocator().getUserService().dtoToEntity( getServiceLocator().getUserService().getUserById(projectDto.getUserId())) );
            projectEntity.setDateStart(projectDto.getDateStart());
            projectEntity.setDateFinish(projectDto.getDateFinish());
        }
        if (!projectEntity.getDescription().equals(projectDto.getDescription()))
            projectEntity.setDescription(projectDto.getDescription());
        if (!projectEntity.getName().equals(projectDto.getName()))
            projectEntity.setName(projectDto.getName());
        if (!projectEntity.getStatus().equals(projectDto.getStatus()))
            projectEntity.setStatus(projectDto.getStatus());
        if (!projectEntity.getUser().getId().equals(projectDto.getUserId()))
            projectEntity.setUser( getServiceLocator().getUserService().dtoToEntity( getServiceLocator().getUserService().getUserById(projectDto.getUserId())) );
        if (!projectEntity.getDateStart().equals(projectDto.getDateStart()))
            projectEntity.setDateStart(projectDto.getDateStart());
        if (!projectEntity.getDateFinish().equals(projectDto.getDateFinish()))
            projectEntity.setDateFinish(projectDto.getDateFinish());

        return projectEntity;
    }

    @Nullable
    @SneakyThrows
    public ru.potapov.tm.dto.Project entityToDto(ru.potapov.tm.entity.Project projectEntity){
        if (projectEntity == null)
            return null;

        ru.potapov.tm.dto.Project projectDto = new Project();
        projectDto.setId(projectEntity.getId());

        if (Objects.nonNull(projectEntity)){
            if (!projectEntity.getDescription().equals(projectDto.getDescription()))
                projectDto.setDescription(projectEntity.getDescription());
            if (!projectEntity.getName().equals(projectDto.getName()))
                projectDto.setName(projectEntity.getName());
            if (!projectEntity.getStatus().equals(projectDto.getStatus()))
                projectDto.setStatus(projectEntity.getStatus());
            if (!projectEntity.getUser().getId().equals(projectDto.getUserId()))
                projectDto.setUserId( projectEntity.getUser().getId() );
            if (!projectEntity.getDateStart().equals(projectDto.getDateStart()))
                projectDto.setDateStart(projectEntity.getDateStart());
            if (!projectEntity.getDateFinish().equals(projectDto.getDateFinish()))
                projectDto.setDateFinish(projectEntity.getDateFinish());
        }

        return projectDto;
    }

}
