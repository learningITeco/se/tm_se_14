package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ITaskRepository;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.Project;
import ru.potapov.tm.dto.Task;
import ru.potapov.tm.repository.IRepository;
import ru.potapov.tm.repository.TaskRepository;
import ru.potapov.tm.util.HibernateToMySql;
import ru.potapov.tm.util.ValidateExeption;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
//@RequestScoped
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ITaskEndpoint")
public final class TaskService extends AbstractService<Task> implements ITaskService {
    @NotNull @Inject private TaskRepository repository;
    public TaskService(@NotNull ServiceLocator serviceLocator, @NotNull IRepository repository) {
        super(serviceLocator);
    }

    @Override
    public int checkTaskSize() throws ValidateExeption{
        int i = 0;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            i =  getRepository().findAll(entityManager).size();
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return i;
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name) throws ValidateExeption{
        @Nullable Task task = null;
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            task = entityToDto(getRepository().findOne(name, entityManager));
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return task;
    }

    @Override
    public void removeTask(@NotNull final Task task) throws ValidateExeption{
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            getRepository().remove(getRepository().findOneById(task.getId(), entityManager), entityManager);
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }

    @Override
    public void removeAllTasksByUserId(@NotNull final String userId) throws ValidateExeption{
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            for (ru.potapov.tm.entity.Task task : getRepository().findAllByUser(userId, entityManager)) {
                if (userId.equals(task.getUser().getId()))
                    getRepository().remove(task, entityManager);
            }
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }

    @Override
    public void removeAllTasks(@NotNull final Collection<Task> listTasks) throws ValidateExeption{
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            for (Task task : listTasks) {
                getRepository().remove(getRepository().findOneById(task.getId(), entityManager), entityManager);
            }
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(name) && Objects.nonNull(getRepository())){
            @NotNull Task newTask = (Task) task.clone();
            newTask.setName(name);
            try {
                entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            getRepository().merge(dtoToEntity(newTask), entityManager);
                entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
            finally {
                try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(project) && Objects.nonNull(getRepository())){
            @NotNull Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            try {
                entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            getRepository().merge(dtoToEntity(newTask), entityManager);
                entityManager.getTransaction().commit();
            }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
            finally {
                try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasksByUserId(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption{
        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            list =  collectionEntityToDto(getRepository().findAllByUserIdAndProjectId(userId, projectId, entityManager));
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return list;
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(String userId) throws ValidateExeption {
        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            list = collectionEntityToDto(getRepository().findAllByUser(userId, entityManager));
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasks(@NotNull final String projectId) throws ValidateExeption{
        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            list =  collectionEntityToDto(getRepository().findAll(entityManager));
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
        return list;
    }

    @Override
    public void putTask(@NotNull final Task task) throws ValidateExeption{
        try {
            ru.potapov.tm.entity.Task entityTask = dtoToEntity(task);
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            if (Objects.isNull( getRepository().findOneById(task.getId(), entityManager)  ))
                getRepository().persist(entityTask, entityManager);
            else
                getRepository().merge(entityTask, entityManager);
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}
    }

    @Override
    public void loadBinar() throws Exception {
    }

    @Override
    public void saveBinar() throws Exception {
    }

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Task task) throws ValidateExeption {
        @NotNull String res = "";
        if (Objects.isNull(task.getDateStart()) || Objects.isNull(task.getDateFinish()) || Objects.isNull(getServiceLocator())
       )
            return res;

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Project " + getServiceLocator().getProjectService().findOneProjectById(task.getProjectId())  + "]" +  "\n";
        res += "    Status: " + task.getStatus() + "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " +  getServiceLocator().getFt().format(task.getDateStart() ) + "\n";
        res += "    Date finish: " + getServiceLocator().getFt().format(task.getDateFinish() ) + "\n";

        return res;
    }

    @NotNull
    @SneakyThrows
    public Collection<ru.potapov.tm.entity.Task> collectionDtoToEntity(@NotNull Collection<ru.potapov.tm.dto.Task> collectionTaskDto){
        Collection<ru.potapov.tm.entity.Task> list = new ArrayList<>();
        for (ru.potapov.tm.dto.Task task : collectionTaskDto) {
            list.add(dtoToEntity(task));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public Collection<ru.potapov.tm.dto.Task> collectionEntityToDto(@NotNull Collection<ru.potapov.tm.entity.Task> collectionTaskEntity){
        Collection<ru.potapov.tm.dto.Task> list = new ArrayList<>();
        for (ru.potapov.tm.entity.Task task : collectionTaskEntity) {
            list.add(entityToDto(task));
        }
        return list;
    }

    @NotNull
    @SneakyThrows
    public ru.potapov.tm.entity.Task dtoToEntity(@NotNull ru.potapov.tm.dto.Task taskDto){
        ru.potapov.tm.entity.Task taskEntity = new ru.potapov.tm.entity.Task();
        taskEntity.setId(taskDto.getId());
        try {
            entityManager = new HibernateToMySql().getEntityManager();
            entityManager.getTransaction().begin();
            taskEntity = getRepository().findOneById(taskDto.getId(), entityManager);
            entityManager.getTransaction().commit();
        }catch (Exception e){e.printStackTrace(); entityManager.getTransaction().rollback();}
        finally {
            try { entityManager.close();} catch (Exception e){e.printStackTrace();}}


        if (Objects.isNull(taskEntity)){
            taskEntity = new ru.potapov.tm.entity.Task();
            taskEntity.setId(taskDto.getId());
            taskEntity = new ru.potapov.tm.entity.Task();
            taskEntity.setDescription(taskDto.getDescription());
            taskEntity.setProject( getServiceLocator().getProjectService().dtoToEntity(getServiceLocator().getProjectService().findOneProjectById( taskDto.getProjectId())));
            taskEntity.setName(taskDto.getName());
            taskEntity.setStatus(taskDto.getStatus());
            taskEntity.setUser( getServiceLocator().getUserService().dtoToEntity(getServiceLocator().getUserService().getUserById(taskDto.getUserId())) );
            taskEntity.setDateStart(taskDto.getDateStart());
            taskEntity.setDateFinish(taskDto.getDateFinish());
        }
        if (!taskEntity.getDescription().equals(taskDto.getDescription()))
            taskEntity.setDescription(taskDto.getDescription());
        if (!taskEntity.getProject().getId().equals(taskDto.getProjectId()))
            taskEntity.setProject( getServiceLocator().getProjectService().dtoToEntity(getServiceLocator().getProjectService().findOneProjectById( taskDto.getProjectId())));
        if (!taskEntity.getName().equals(taskDto.getName()))
            taskEntity.setName(taskDto.getName());
        if (!taskEntity.getStatus().equals(taskDto.getStatus()))
            taskEntity.setStatus(taskDto.getStatus());
        if (!taskEntity.getUser().getId().equals(taskDto.getUserId()))
            taskEntity.setUser( getServiceLocator().getUserService().dtoToEntity(getServiceLocator().getUserService().getUserById(taskDto.getUserId())) );
        if (!taskEntity.getDateStart().equals(taskDto.getDateStart()))
            taskEntity.setDateStart(taskDto.getDateStart());
        if (!taskEntity.getDateFinish().equals(taskDto.getDateFinish()))
            taskEntity.setDateFinish(taskDto.getDateFinish());

        return taskEntity;
    }

    @Nullable
    @SneakyThrows
    public ru.potapov.tm.dto.Task entityToDto(@Nullable ru.potapov.tm.entity.Task taskEntity){
        if (taskEntity == null)
            return  null;

        ru.potapov.tm.dto.Task taskDto = new Task();
        taskDto.setId(taskEntity.getId());

        if (Objects.nonNull(taskEntity)){
            if (!taskEntity.getDescription().equals(taskDto.getDescription()))
                taskDto.setDescription(taskEntity.getDescription());
            if (!taskEntity.getProject().getId().equals(taskDto.getProjectId()))
                taskDto.setProjectId(taskEntity.getProject().getId());
            if (!taskEntity.getName().equals(taskDto.getName()))
                taskDto.setName(taskEntity.getName());
            if (!taskEntity.getStatus().equals(taskDto.getStatus()))
                taskDto.setStatus(taskEntity.getStatus());
            if (!taskEntity.getUser().getId().equals(taskDto.getUserId()))
                taskDto.setUserId( taskEntity.getUser().getId() );
            if (!taskEntity.getDateStart().equals(taskDto.getDateStart()))
                taskDto.setDateStart(taskEntity.getDateStart());
            if (!taskEntity.getDateFinish().equals(taskDto.getDateFinish()))
                taskDto.setDateFinish(taskEntity.getDateFinish());
        }

        return taskDto;
    }

    //Save-Load
    @Override public void saveJaxb(boolean formatXml) throws Exception { }
    @Override public void loadJaxb(boolean formatXml) throws Exception {}

    @Override public void saveFasterXml() throws Exception { }
    @Override public void loadFasterXml() throws Exception {}

    @Override public void saveFasterJson() throws Exception { }
    @Override public void loadFasterJson() throws Exception {}
}
