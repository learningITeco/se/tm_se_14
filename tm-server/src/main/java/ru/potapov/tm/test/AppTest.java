package ru.potapov.tm.test;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Getter
@Setter
@NoArgsConstructor
//@ApplicationScoped
public class AppTest {
    @Inject
    Test1 test1;
    public void init(){
        System.out.println("OK");
        System.out.println(test1);
    }
}
