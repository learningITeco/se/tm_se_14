package ru.potapov.tm.test;

import ru.potapov.tm.Application;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.repository.ProjectRepository;

import javax.enterprise.inject.se.SeContainerInitializer;
import javax.inject.Inject;
import javax.inject.Named;

public class Test {
    @Named("ProjectRepository")
    //@Inject
    ProjectRepository projectRepository;
    public static void main(String[] args) {
        try {
            SeContainerInitializer.newInstance()
                    .addPackages(Test.class).initialize()
                    .select(Test.class).get().start();
        }catch (Exception e){ e.printStackTrace(); }

    }

    public void start(){
        System.out.println("OK");
    }

    public void init(){
        System.out.println("INIT");
    }
}
