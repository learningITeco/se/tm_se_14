package ru.potapov.tm.util;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;

import javax.enterprise.context.*;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionScoped;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Getter
@Setter
@NoArgsConstructor
@ApplicationScoped
public class HibernateToMySql implements Serializable {
    @NotNull private Properties propertyService   = new Properties();
    @Nullable private FileInputStream stream;
    @Nullable private EntityManagerFactory factory = factory();


    //@NotNull private EntityManager entityManager;

    private EntityManagerFactory factory() {
        String resource = "mybatis-config.xml";
        try {
            stream = new FileInputStream(resource);
            propertyService.load(stream);
        }catch (Exception e){e.printStackTrace();}

        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getProperty("driver", "com.mysql.jdbc.Driver"));
        settings.put(Environment.URL, propertyService.getProperty("url", "jdbc:mysql://localhost:3306/tm"));
        settings.put(Environment.USER, propertyService.getProperty("user", "root"));
        settings.put(Environment.PASS, propertyService.getProperty("password", "root"));
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        //sources.addAnnotatedClass(Cat.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }


    @Produces
    @PersistenceContext
    @ru.potapov.tm.annotation.EntityManager
    //@TransactionScoped
    //@Dependent
    public EntityManager getEntityManager() {
        return factory.createEntityManager();
    }

    public void close(@Disposes @Default EntityManager entityManager){
        if (entityManager.isOpen()){
            entityManager.close();
        }
    }
}
